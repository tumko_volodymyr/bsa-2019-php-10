<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('login', 'API\UserController@login');
Route::post('register', 'API\UserController@register');

Route::get('/items', 'MarketApiController@showList');
Route::get('/items/{id}', 'MarketApiController@showProduct');

Route::group(['middleware' => 'auth:api'], function(){
    Route::post('/items', 'MarketApiController@store');
    Route::delete('/items/{id}', 'MarketApiController@delete');
});
