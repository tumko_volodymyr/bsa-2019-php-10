<?php

namespace App\Http\Controllers;

use App\Entities\Product;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use App\Services\MarketService;

class MarketWebController extends Controller
{
    private $marketService;

    public function __construct(MarketService $marketService)
    {
        $this->marketService = $marketService;
    }

    public function showMarket() {
        $products = $this->marketService->getProductList();

        return view('market', compact('products'));
    }

    public function showProduct(int $id) {

        $product = $this->marketService->getProductById($id);

        if (null === $product){
            abort(404);
        }

        return view('product', compact('product'));
    }

    public function addProductForm() {
        return view('addProductForm');
    }

    public function storeProduct(Request $request) {

        try{
            $this->authorize('create', Product::class);
        }catch (AuthorizationException $exception){
            return redirect()->route('main');
        }
        $this->marketService->storeProduct($request);

        return redirect()->route('main');
    }

    public function deleteProduct(Request $request, $id) {

        $product = $this->marketService->getProductById($id);

        if (null === $product){
            abort(404);
        }

        try{
            $this->authorize('delete', $product);
            $this->marketService->deleteProduct($request);
        }catch (AuthorizationException $exception){
            return redirect()->route('main');
        }

        return redirect()->route('main');
    }
}
