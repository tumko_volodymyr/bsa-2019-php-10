<?php

namespace App\Http\Controllers;

use App\Entities\Product;
use App\Http\Resources\ProductResource;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Services\MarketService;

class MarketApiController extends Controller
{
    private $marketService;

    public function __construct(MarketService $marketService)
    {
        $this->marketService = $marketService;
    }

    public function showList() {
        $products = $this->marketService->getProductList();

        return $products->map(function ($product){
            return new ProductResource($product);
        });
    }

    public function store(Request $request) {
        try{
            $this->authorize('create', Product::class);
        }catch (AuthorizationException $exception){
            return $this->createJSONResponse([], 403);
        }

        $product = $this->marketService->storeProduct($request);
        return (new ProductResource($product))
            ->response()
            ->setStatusCode(201);
    }

    public function showProduct(int $id) {
        $product = $this->marketService->getProductById($id);

        if (null === $product){
            return $this->createJSONResponse([], 404);
        }

        return new ProductResource($product);
    }

    public function delete(Request $request, $id) {
        $product = $this->marketService->getProductById($id);

        if (null === $product){
            return $this->createJSONResponse([], 404);
        }

        try{
            $this->authorize('delete', $product);
            $this->marketService->deleteProduct($request);
            return $this->createJSONResponse([], 204);
        }catch (AuthorizationException $exception){
            return $this->createJSONResponse([], 403);
        }

    }

    private function createJSONResponse (array  $data, int  $code){
        return (new JsonResponse($data, $code));
    }
}
