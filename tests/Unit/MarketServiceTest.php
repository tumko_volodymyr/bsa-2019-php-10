<?php


namespace Tests\Unit;


use App\Entities\Product;
use App\User;
use App\Repositories\ProductRepository;
use App\Services\MarketService;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Tests\TestCase;

class MarketServiceTest extends TestCase
{
    private $repositoryStub;
    private $marketServiceStub;

    protected function setUp(): void
    {
        parent::setUp();
        $this->repositoryStub = $this->createMock(ProductRepository::class);
        $this->marketServiceStub = new MarketService($this->repositoryStub);
    }

    public function testGetProductList()
    {
        $this->repositoryStub->method('findAll')->will(
            $this->onConsecutiveCalls(new Collection(), new Collection([new Product()]))
        );

        $this->assertInstanceOf(Collection::class, $this->marketServiceStub->getProductList());

        $this->assertInstanceOf(Collection::class, $products = $this->marketServiceStub->getProductList());
        $this->assertNotEmpty($products);
        foreach ($products as $product){
            $this->assertInstanceOf(Product::class, $product);
        }
    }

    public function testGetProductById()
    {
        $this->repositoryStub->method('findById')->will(
            $this->onConsecutiveCalls(new Product(), null)
        );

        $this->assertInstanceOf(Product::class, $this->marketServiceStub->getProductById(1));
        $this->assertNull($this->marketServiceStub->getProductById(1));
    }

    public function testGetProductsByUserId()
    {
        $this->repositoryStub->method('findByUserId')->will(
            $this->onConsecutiveCalls(new Collection(), new Collection([new Product()]))
        );

        $this->assertInstanceOf(Collection::class, $this->marketServiceStub->getProductsByUserId(1));

        $this->assertInstanceOf(Collection::class, $products = $this->marketServiceStub->getProductsByUserId(1));
        $this->assertNotEmpty($products);
        foreach ($products as $product){
            $this->assertInstanceOf(Product::class, $product);
        }
    }

    public function testStoreProduct()
    {
        $this->repositoryStub->method('store')->will(
            $this->returnArgument(0)
        );
        $user = factory(User::class)->make(['id'=>3]);
        $this->actingAs($user);

        $request = $this
            ->getMockBuilder(Request::class)
            ->getMock();

        $productName = 'name';
        $productPrice = 54.3;
        $request
            ->expects($this->any())
            ->method('input')
            ->will($this->returnValueMap([
                    ['name', null, $productName],
                    ['price', null, $productPrice ]
                ])
            );

        $this->assertInstanceOf(Product::class, $product = $this->marketServiceStub->storeProduct($request));

        $this->assertEquals($productName, $product->name);
        $this->assertEquals($productPrice, $product->price);
        $this->assertEquals($user->id, $product->user_id);

    }

    public function testDeleteProduct()
    {
        $product = factory(Product::class)->make(['id'=>3]);
        $this->repositoryStub->expects($this->once())->method('delete')->with(
            $this->logicalAnd(
                $this->isInstanceOf(Product::class),
                $this->callback(function($arg) use ($product) {
                    return $arg->id === $product->id ;
                })
            )
        );

        $this->repositoryStub->method('findById')->will(
            $this->onConsecutiveCalls($product, null)
        );

        $request = $this
            ->getMockBuilder(Request::class)
            ->getMock();

        $request
            ->expects($this->any())
            ->method('__get')
            ->with('id')
            ->willReturn($product->id);

        $this->marketServiceStub->deleteProduct($request);
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('No product with id: '.$request->id);
        $this->marketServiceStub->deleteProduct($request);
    }
}
