<?php


namespace Tests\Feature;


use App\Entities\Product;
use App\User;
use Tests\TestCase;

class TaskTest extends TestCase
{

    const JSON_STRUCTURE = [
            'id',
            'name',
            'price',
            'user_id'
        ];

    public function setUp(): void
    {
        parent::setUp();
        $this->artisan('migrate:fresh --seed');
        $this->artisan('passport:install');
    }

    public function test_items()
    {
        $response = $this->json("GET", "/api/items");

        $response->assertOk()
            ->assertJsonStructure(['*' => self::JSON_STRUCTURE]);;

    }

    public function test_item()
    {
        $response = $this->json("GET", "/api/items/3");

        $response->assertOk()
            ->assertJsonStructure(self::JSON_STRUCTURE);

    }

    public function test_store()
    {
        $user = User::first();
        $token =  $user->createToken('MyApp')-> accessToken;
        $response = $this->json("POST", "/api/items", [
            'name' => 'Apple MacBook444',
            'price' => 4999.99
        ], ['AUTHORIZATION' => 'Bearer '.$token]);

        $response->assertStatus(201)
            ->assertJsonStructure(self::JSON_STRUCTURE);

    }

    public function test_store_un_auth()
    {
        $response = $this->json("POST", "/api/items", [
            'name' => 'Apple MacBook444',
            'price' => 4999.99
        ]);
        $response->assertStatus(401);

    }

    public function test_delete_un_auth()
    {
        $response = $this->json("DELETE", "/api/items/3");
        $response->assertStatus(401);
    }

    public function test_delete()
    {
        $user = User::first();
        $product = Product::where(['user_id' => $user->id ])->first();
        $token =  $user->createToken($user->id)->accessToken;
        $response = $this->json('DELETE', '/api/items/' .$product->id, [], ['AUTHORIZATION' => 'Bearer '.$token]);
        $response->assertStatus(204);
    }

    public function test_delete_forbiden()
    {
        $product = Product::orderBy('id', 'desc')->get()->first();

        $user = User::first();
        $token =  $user->createToken($user->id)->accessToken;
        $this->assertNotEquals($user->id, $product->user_id);

        $response = $this->json('DELETE', '/api/items/' .$product->id, [], ['AUTHORIZATION' => 'Bearer '.$token]);
        $response->assertStatus(403);
    }

}
