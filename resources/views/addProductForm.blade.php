@extends('layouts.app')

@section('content')
<div class="row justify-content-center">
    <form method="POST" action="{{ route('store') }}">
        @csrf

        <label for="name">Product name:</label><br>
        <input id="name" name="name" type="text" value="{{ old('product_name') }}" required><br>

        <br/>

        <label for="price">Product price:</label><br>
        <input id="price" name="price" type="number" step="0.01" min="0" value="{{ old('product_price') }}" required><br>

        <hr/>

        <div style="text-align: center">
            <button type="submit" class="btn btn-primary">Add</button>
        </div>

    </form>
</div>
@endsection
